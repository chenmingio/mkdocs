# HtDP - Background 背景介绍

目前的许多编程课程强调先做后学,在做中设计.MVP,先使用直觉构造程序.

Htdp的理念略有不同,更强调使用一套设计方法论.HtDP的作者们把软件的设计推为一种艺术形式,需要按照严格的程序/系统思维来设计.且不论是否准确,这套设计方法的确有其合理高效之处.

我的学习路径以edX的课为主,htdp官网文档为辅助和习题库.

软件设计在分为function design/data design/world design.

## 参考文献

[How to Design Programs, Second Edition](https://htdp.org/2018-01-06/Book/)

[Course | HtC1x | edX](https://courses.edx.org/courses/course-v1:UBCx+HtC1x+2T2017/course/)

[Design Recipes | HtC1x | edX](https://courses.edx.org/courses/course-v1:UBCx+HtC1x+2T2017/77860a93562d40bda45e452ea064998b/#S1)

