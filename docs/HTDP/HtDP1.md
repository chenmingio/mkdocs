# HtDP - How To Design Function 如何设计function

## WHY?

根据我之前培训班的体会,同样的功能,有的人用10行就能实现;有的人需要30行加3层嵌套.有的人用10分钟快速实现,有的人需要10小时敲敲补补.

我们阅读了很多具体语言的语法和API,但对于构造函数结构的层级,大部分时间是靠直觉、经验.

有没有一套方法论,可以提高我们在函数构造能力?(更快、更简洁、更稳固).

## HOW?

解决问题的一种思路是separate/分离.每次完成一个层面的问题,逐一击破. 

HTML/CSS/JS分解了网页的内容/风格/交互,提高了网页制作的效率.

React又使用JSX把HTML/JS合在一起,称前者为xx分离,后者为concern分离.新的分离方法又提高了UI开发的效率.

在函数构造这个步骤上,可以分离为这些维度:

- 函数吃进的数据类型是什么?函数的目的是什么?
- 函数吐出的数据类型是什么?
- 如何使用代码验证函数的有效和完备?
- 函数的大致形态如何
- 最后的最后,再思考:函数内部如何实现以上的构想.

而实际中我这样写函数:

- 抓住脑海里蹦出的第一个念头
- 直接开始写函数内部
- 在上面这个脚手架上敲敲打打
- 思考主要围绕在具体语法和API使用上
- 写完后丢进上下文环境开始debug,反复试错
- 不思考完备性,埋下隐患
- 不写函数注释,或者写得没头没脑,后来者无法理解.

高下立判.合理的函数构造方法/习惯是一套思维框架,看似教条刻板,但熟练使用内化以后,可以提高效率,避免给自己挖坑.

下面就是具体的步骤,建议读者注册课程后阅读构造指南(写得非常详细),以官方文档为主要学习资料.下面内容只是我自己的想法和提炼.

## 构建函数 function design

  - Form Problem Analysis to Data Definitions   
  - Signature, Purpose Statement, Header
  - Functional Examples
  - Function Template
  - Function Definition
  - Testing

### Form Problem Analysis to Data Definitions   

设计函数的前一步,设计数据.第2章会讲.

### Signature

input -> output. 输入输出的数据类型尽可能详细具体（integer/nature/float/customized type）. Type用首字母大写表示Class.

### Purpose Statement

一句话描述函数的目的.比如: produce xxx of xxx

### Stab

构造一个语法上完整的函数外壳,返回一个常值.主要包括函数的名字,变量的名字,数量.目的是提供脚手架,确保函数的syntax正确(可以被运行/构造正确).

用系统跑一遍,解决语法错误.提早发现函数‘外壳’的错误.

lisp这么写:

`(define (f_name arg) 0) ; this is the stub`

用python表示:

```
def f_name(arg):
	return 0/"a"/xx	
```

### Functional Examples

- 函数例子的目的是帮助你明确函数的目的.比Purpose Statement更形式化和具体.Purpose Statement是需求方提出的自然语言描述,Functional Examples则是基于stab外壳上的程序语言需求描述.

例子包括输入和输出两部分.有时候输出不一定是结果,本身也可以用函数来表达.

要为几种典型或边界情况写例子.因为根据输入数据的不同,函数内部的运算可能也完全不同,所以在example这一步就要列好.

神奇的是,课程把example和测试功能结合起来,你写下的每个例子都能在最后帮助你反过来检验自己函数的准确和完备.一举两得!

rocket使用check-expect检验.

lisp:

`(check-expect (f_name arg) result)`

python:(先自己造个check-expect函数)

`check-expect(f_name(arg), result)`


check expect的对比项目不但要显示最终的结果,还要反应这个结果是如何产生的.只有这样,也通过写check-expect才可以整理思路.


### Function Template

从data driven templates里拷贝过来.

同时加入可能需要的constant常数.

不要把常数直接用在函数里!

不要把常数直接用在函数里!

不要把常数直接用在函数里!

写在函数的头部!避免magic number问题.我发现公司里员工做报表效率极低的原因之一就是把常数写在公式/报告里,回头一下要改很多地方,甚至自己都忘了哪些地方要改.



### 其他

- 整个loop是循环迭代的脚手架，并非一次成型。有时候你没想好具体的输入输出数据类型,可以先用example写几个例子,让自己的思维快速成型.
- run early and run often 在写完stab的时候,rocket就可以测试了.这里其实就是快速迭代,快速获取反馈,尽早发现错误,以提高后续行为准确性,最终提高效率的思想.生活和工作里也可以用这个方法提高行为的效率和准确度.


## Log
2019-04-20 把课后习题都做了一遍,发现上次的理解很多不准确,重写后顺畅多了.




