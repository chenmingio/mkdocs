# How to design World 如何设计世界

template是你从编程工作开始就一直要使用的框架.它如同脚手架,帮助你完成从领域知识到代码实现之间的转化过程.

## 步骤

### 为每一个senario画一份手稿

画手稿的一个目的是整理要素,而分senario画手稿帮助分类整理要素.

一般包括
* 最原始的状态.这其中主要就涉及constant变量
* 交互中发生的内容(主要涉及变量)
* 交互中的mechanism(主要设计系统提供的method)

### 设置constant variable

先随便给一个数字,回头再改成定植(快写慢改).但另一方面,与某个常数相关联的另一个常数,则使用关联式.(视频里称之为single point of control,单变量降低**复杂度**).

举个例子: 你需要一个正方形,一种方法是square(width, width).另一种是square(length, width, length = width).显然后一种方法更future-proof.设计的时候使用更普遍的形态,以应对未来的变更需求.

单元测试里的预测结果部分也要使用常数变量,减少hardcode.

### 设置data

data就是整个程序里会变动的variable.确定尽可能少的variable,明确定义这个data,其他的function都围绕这个data展开工作.

相同的data definition,对应的function往往也结构类似.

### 设计function

一开始就需要对main function和下面主要function的功能有大致的构想. main里面有哪些function,名字叫什么,他们的输入输出数据类型都是什么.给它们每个function都建立一个stub,并用TODO或者!!!标记为未完成.

### 画模型

好的设计者有大量的模型,有些模型涉及到高等数学模型.

### Traceability

设计手稿里的组件与代码里的组件一一对应,以降低复杂度.之后增添功能,也需要先在设计手稿里增加对应组件.背后原理是,设计手稿和程序组件一一对应,在设计手稿里的思考更直观,所以现在这个层面完成一部分设计.

### 常数和traceablity的目的

世界上有两种程序:不断改变/改进,和其他.设置常数和保存traceability的目的,就是让改变程序更容易.

### 不要节省example/check-expect的步骤


## 如何更新你的程序

从头执行相同的流程,先更新手稿,然后在对应的代码出加入更新的内容.

### 起名字的习惯

内建或自建的数据类型首字母大写,例如Cat/Cow/Box...

数据作为函数参数时,则可以简单,用小写字母代替,例如c/c/b/ke(key event)

有时候函数的命名可以是这个函数广义上的功能,而不是具体上下文中的功能.例如render代替render text.

描述函数的功能时,最好可以描述它实现的方法,而不只是对现象/需求进行简单描述.这样也可以帮助阅读的人理解你下面的操作主要包括了哪些.

Boolean变量或者function,名字可以之间带一个?比较大小可以使用bigger?

