# 自指data与自指函数

在data-driven-programming里,嵌套函数的魔力来自于自指的数据定义.

目前课程主要使用cons列表作为自指数据类型.

自指函数可以简洁优雅地解决复杂的conditonal问题,让人不禁怀疑,“普通”函数是直指函数的特殊情况.

## 如何构造自指函数?

首先你需要构建自指数据类型,其中包括一个base case和一个自指结构.template如下:

```
;; ListOfString is one of:
;;  - empty
;;  - (cons String ListOfString)
;; interp. a list of strings

(define LOS-1 empty)
(define LOS-2 (cons "a" empty))
(define LOS-3 (cons "b" (cons "c" empty)))

#;
(define (fn-for-los los)
  (cond [(empty? los) (...)]                   ;BASE CASE
        [else (... (first los)                 ;String
                   (fn-for-los (rest los)))])) 
                   
;;             /
;;            /
;;       COMBINATION
;; Template rules used:
;;  - one of: 2 cases
;;  - atomic distinct: empty
;;  - compound: (cons String ListOfString)
;;  - self-reference: (rest los) is ListOfString
```



## 我的理解


### 1

因为函数式编程一个function只能执行一次,所以要想办法把各种function串联为一个function.

例如render一个普通图像只要:

```
(define (render img)
    (place-image img background))
```

但要render一连串嵌套的图像(list of image LOI)时,一个参数可能就不够用了,因为无法把下一个图像也包括进来.

```
(define (render first-img next-img)
    (place-image first-img next-img))
```

### 2

核心部分是解决列表中第一个对象和后面自指数据结果的关系(例子中String和后面fn-for-los(ListOfString)输出结果的关系).有时候是两者相加,这样你就得到了整个list的和.有时候两者共同构建一个新的数列,这样你就可以给数列排序.
