# 辅助函数 Helper Function

Function的作用之一是抽象,把一堆表达式用函数封装起来,带来的好处是可以复用.

那如果不需要复用时,我们就不需要抽象了嘛?显然另一个好处:合理的分割有利于编写和阅读.这样,主函数只留下高层次的对象.下位具体的处理留在对象的子函数里.

## 比较两种helper function

### 1

```
(define (arrange-images loi)
  (layout-images (sort-images loi)))
  ; end
  ```
类似于wrapper

### 2

```
(define (arrange-images loi)
  (layout-images (sorted(first loi))
      .... arrange-images (sorted (rest loi))))
      ; template for natural recursion
```

### 使用helper function的时机

* 领域操作对象变换时(例如从操作列表转为操作image/string等)
* 当自任务需要处理列表数据时
* 记住原则:一个function解决一个问题.如果你的函数可以在理解上被分为两个,你就可以这么做.

### 练习总结

这部分的联系难度突然增高.即使习题中帮你把70%的内容都写好了,也需要2小时才能想出剩下的部分.

思考的路径可能包括

* 把整个问题拆成最小的组块(函数)
* 根据逻辑,组合最小函数
  * 有些组合是wrapper式(嵌套)
  * 有些是serial(前后并置)
  * 有时需要确认是否要用natural recursion还是简单wrapper,如上文例子所示.
* 总的来说,还是先要确定好逻辑,把需求分解组合成简单需求,再动手写代码.
