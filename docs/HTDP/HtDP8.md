# Mutual Ref

mutual ref数据类型是Ref数据类型的进化,也就是compound数据中增加了一个属性,而这个属性恰恰是一个列表,而且是compound数据的列表.

最常见的应用就是有parent-child的数据,例如文件夹(文件里有文件和文件夹),例如家族族谱(成员有子代集合,子代集合本身也包含成员).

和ref数据类型相似,你也需要构造两套function template,写2行function signature,写两套单元测试.


## 经验

如果你的函数设计有前提,请写在注释里.
