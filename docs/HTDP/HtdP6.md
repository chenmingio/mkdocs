# 复合自指data


符合自指函数一般就是一组列表,列表的每个元素是几个相关数据的复合体.

这样你就有有两个data definition,两个function template.

第一个data是compound data,他的function里需要包含对每个属性的操作.

第二个data是natural recursion循环,它需要对单个compound data进行操作,也就是使用上面一个function的template.

## 问题/现象

有一组学校,每个学校有两个属性.

### 一种data设计:

单独学校为一个复合data,名叫School

```
(define-struct school (name tuition))
```

同时用一个自指列表保存每个School

```
;; ListOfSchool is one of:
;;  - empty
;;  - (cons School ListOfSchool)
```

### 另一种data设计

只有一个符合data,也叫School.但School本身又自指了自己.所以School一共三个属性(名字、学费、下一个School).作用于School的函数,也会作用于下一个函数.所以只要一个函数就可以解决需求.

```
(define-struct school (name tuition next))
```

## 一些经验

设计单元测试时,也要避免hardcode,不是把最终的结果放上去,而是要体现你期待最终是如何操作数据.
