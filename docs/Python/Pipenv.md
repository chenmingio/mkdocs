# Pip and pipenv

pipenv适用人群:

> While pip alone is often sufficient for personal use, Pipenv is recommended for collaborative projects as it’s a higher-level tool that simplifies dependency management for common use cases. 
> -- python官网



## Pip简介

pip可以安装pypi上的package。pypi上的package千千万，总有一款适合你，这样就不用自己造轮子了。

初始情况下使用pip3 install [package]安装包，package会集中储存在/usr/local/lib/python3x/site-packages里。

使用pip3 install [package] --user时，会存在用户单独的/Users/chenming/Library/Python/3.7/lib/python/site-packages文件夹里，这样据说不会污染系统自带的package库。

### pip其实挺好用。

你可以用pipfreeze导出你电脑上pip库里安装的package到一个txt文件，别人拿着你的txt文件也可以一次安装所需的package。

### 问题是。。。

1. 你不同程序需要的package不同，但pip freeze一次导出所有的package，别人怎么知道真正需要的最小package集合呢。
2. 不同项目所需要的package版本不同，有兼容问题。

### 另外一方面。。。
开发需要使用虚拟环境，把你的项目文件夹变成隔离的环境，这样你升级python的时候老项目还能继续使用。（这么说一个项目一台主机是不是最好/壕的办法？或者在一个主机上装几个隔离系统。所以要思考在什么层级上进行隔离最适合自己。）

一般会使用virtualenv管理环境：在项目文件夹创建虚拟环境，里面有单独的package环境，单独的python环境或更低层的环境。你在里面单独安装所需要的package，导出的package列表也是最小的。（package到底安装在了哪里？？？）

### 为什要选pipenv
pipenv之前，你需要**同时**设置上面提到的各种层级上的环境，具体来说有四种任务：

1. 管理package，导出package列表。
2. 建立虚拟环境
3. 管理各个的虚拟环境
4. 不同的管理工具有不同的命令

pipenv宣称一次能解决这四个问题及合作开发中的其他问题（快使用pip牌洗发水。。。）

下面让我们看看他是如何做到的，效率高不高。

## Pipenv的工作原理

### 1. pipenv install [package]

* pipenv创建了虚拟环境，在.local/share里面。为啥？
* pipenv在项目文件里新建了pipfile，用来记录文件夹里安装的package清单，相当于以前的requirement.txt文件。
* pipenv使用系统的python创建了python环境（？？系统的哪个python？）
* pipenv在xxx里安装了[package]
* pipenv在项目文件夹里新建里piplock，详细记录了环境/package/dependency




## Pipenv的日常使用命令

创建好pipenv环境后进入virtualenv环境：

pipenv shell

退出

ctrl d 


### 使用pipenv运行程序

pipenv run python hello.py

### 在现有文件夹里开始使用pipenv

pipenv install requirements.txt

### Find out what’s changed upstream: 
$ pipenv update --outdated.

### Upgrade packages, two options:
1. Want to upgrade everything? Just do $ pipenv update.
2. Want to upgrade packages one-at-a-time? $ pipenv update <pkg> for each outdated package.

### pipenv shell
？？

## Pipenv安装的两种方法

### python.org推荐

pip install --user pipenv
需要解决PATH变量问题


## 参考文献

Official Repo 

[pypa/pipenv: Python Development Workflow for Humans.](https://github.com/pypa/pipenv)



Python官网对pip和pipenv的介绍和使用方法

[Installing Packages — Python Packaging User Guide](https://packaging.python.org/tutorials/installing-packages/)

[Managing Application Dependencies — Python Packaging User Guide](https://packaging.python.org/tutorials/managing-dependencies/)

视频教程1，同时介绍了pipfile的阅读/使用方法

[(1) Python Tutorial: Pipenv - Easily Manage Packages and Virtual Environments - YouTube](https://www.youtube.com/watch?v=zDYL22QNiWk&t=295s)



[Why Python devs should use Pipenv | Opensource.com](https://opensource.com/article/18/2/why-python-devs-should-use-pipenv)

[Basic Usage of Pipenv — pipenv 2018.11.27.dev0 documentation](https://pipenv.readthedocs.io/en/latest/basics/)

反对pipenv的一篇文章：

[Pipenv: promises a lot, delivers very little | Chris Warrick](https://chriswarrick.com/blog/2018/07/17/pipenv-promises-a-lot-delivers-very-little/)

> Pip, setup.py, and virtualenv — the traditional, tried-and-true tools — are still available, undergoing constant development. Using them can lead to a simpler, better experience. Also of note, tools like virtualenvwrapper can manage virtualenvs better than the aforementioned new Python tools, because it is based on shell scripts (which can modify the enivironment).


如何使用virtualenv

[Installing packages using pip and virtualenv — Python Packaging User Guide](https://packaging.python.org/guides/installing-using-pip-and-virtualenv/)

## 思考与后记

发现我作为独立开发者，目前不需要pipenv，所以卸载了。全剧终，谢谢观看！

### 学到的元技能：

* 工具适用范围不同，先搞清楚适用人群。
* 先读读python官网怎么评价这个工具
* 安装的时候读读安装报告，信息挺多
* 安装方法还挺有讲究
* 查查反对者的声音




## log
20190217 2h 初稿










使用—user安装pipenv，把user目录添加到PATH里

确保pipenv/venv文件不会被git同步=>gitignore generator


Pipenv文件的一些作用

It automatically creates and manages a virtualenv for your projects, as well as adds/removes packages from your Pipfileas you install/uninstall packages. It also generates the ever-important Pipfile.lock, which is used to produce deterministic builds.

同时取消了requirements

venv的原理




The Pipfile is used to track which dependencies your project needs in case you need to re-install them, such as when you share your project with others. 


pipenv install requests

Official Repo
[pypa/pipenv: Python Development Workflow for Humans.](https://github.com/pypa/pipenv)



