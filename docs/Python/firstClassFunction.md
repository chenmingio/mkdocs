# function in python

## 函数是什么

### 创建函数

```
def 函数(参数):
	return xxx参数xxx
```

发生了什么:

python使用def语句来**创建**函数.具体说:

python创建了一个叫foo的变量,它指向一个foo(arg)的函数对象.图解如下:

![pythonLive](firstClassFunction.png)


### 使用函数

```
result = foo("hello world")
```
发生了什么:

1. python把return中的arg用具体的“hello world”替换.即xxxhello worldxxx
2. 运行替换后的表达式 xxxhello worldxxx
3. 把运行后的结果返回给result

### 当我们讨论函数的时候,需要区分讨论对象是变量foo,函数foo(arg),还是运算结果foo(‘hello world“).



## 函数增强

我想增强下面这个函数,它每次运算能额外增加一个功能(比如打印一句话).

**创建**老函数:

```
def 老函数(参数):
    return xxx(参数)xxx # 运算结果
```


解决方法:

创建一个新函数,它在运行的时候,插入一个新功能.新函数就成了增强型的老函数.

新函数使用的还是老函数的[参数arg],只是运行的时候有额外动作.

形式如下

```
def 新函数(参数):
    新功能
    return 老函数(参数) # 这是老函数的运算结果 
    
    #注意return放最后,return之后表达式不会被执行
```

这种语法能够成功的前提是:外层函数的参数可以有效传递到内部函数中.

## 函数制造机

又一天,你需要为每个新函数都加上一句话.(很常见的的功能,你希望每个函数运行的时候都print一句:“xxx函数的运行结果是yyy”)

大概有100个需要改造的老函数.意味着你需要创建100个新函数.

这时候你开始幻想一个函数制造机,它能吃进老函数,吐出新函数.(函数的函数,元函数)

幸运的是,函数在python也是对象,你完全可以这么做.

函数制造机原理如下: 

```
def 函数制造机(老函数):
	def 新函数(参数):
		增强功能
		return 老函数(参数)  # 老函数运算结果
	return 新函数 # 函数制造机的运算结果
```

第一行和最后一行好理解,就是我们的需求.

第二行到第四行和*函数增强*里的**内容一样**.只是这次它作为函数制造机的运算结果被返回.

a new level of abstraction!

## 装饰器

当你能理解上面的原理时,装饰器就很简单了.装饰器不过是使用函数制造机制造新函数,然后覆盖老函数的名字. 

```
@函数制造机
def 老函数(参数)
	return 老函数运算参数结果
```
这个形式将返回新函数.

## 下一步学习:

- 看懂了是第一步,请自己动手做几个简单的装饰器
- 目前的增强函数没有自己的参数.如何在增强函数中增加新参数?(就可以小幅度调整增强效果)
- 目前的函数制造机没有自己的参数.如何在函数制造机中增加新参数?(就可以大幅度调整增强效果)
- bottle/flask使用了带参数的函数制造机.找到代码并说出其原理.


## 个人发现

* 看来计算的运行总是改变一个对象的状态.即使print这种看似不返回结果的语句,其实是把结果返回给了terminal的命令行
* 函数return后面总是跟着一个计算结果.计算结果是对象,对象可以是string对象,也可以是函数对象


## 参考文献

[Live Programming Mode - Python Tutor - Visualize Python and JavaScript code](http://www.pythontutor.com/live.html#mode=edit)

[Programming Terms: Closures - How to Use Them and Why They Are Useful - YouTube](https://www.youtube.com/watch?v=swU3c34d2NQ&t=6s)

[Programming Terms: First-Class Functions - YouTube](https://www.youtube.com/watch?v=kr0mpwqttM0)

[python decorators - YouTube](https://www.youtube.com/results?search_query=python+decorators&page=&utm_source=opensearch)


