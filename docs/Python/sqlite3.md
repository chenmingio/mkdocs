Sqlite3

## 背景知识

sqlite不需要独立的服务器部分
sqlite的语法有些非标准部分
sqlite可以方便地迁移到PostgreSQL里


准备长时间使用split3。需要熟悉下

* general SQL 命令
* sqlite3的特点和特殊用法



## 创建db/table/
import sqlite3

### Create table records

#### sqlite简单做法

```
cursor.execute('''CREATE TABLE records (T0, T1, duration, activity)''')

db.commit()
```

#### general SQL做法



## 日常连接数据库

### 创建链接connection（或创建）
conn = sqlite3.connect('timelogger.db')

connection对象特点： [sqlite3 — DB-API 2.0 interface for SQLite databases — Python 3.7.2 documentation](https://docs.python.org/3.7/library/sqlite3.html#sqlite3.Connection)

绝对/相对链接实例：

###  创建cursor
cursor = conn.cursor()

cursor对象描述：[sqlite3 — DB-API 2.0 interface for SQLite databases — Python 3.7.2 documentation](https://docs.python.org/3.7/library/sqlite3.html#sqlite3.Cursor)

### 创建table

sqlite3方法

```
# Create table
c.execute('''CREATE TABLE stocks
             (date text, trans text, symbol text, qty real, price real)''')
             
# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
```

标准方法


## 在python中使用变量的方法

推荐使用DB-API方法:
?占位 + tuple (value,)作为execte的第二个参数

```
# Do this instead
t = ('RHAT',)
c.execute('SELECT * FROM stocks WHERE symbol=?', t)
print(c.fetchone())

# Larger example that inserts many records at a time
purchases = [('2006-03-28', 'BUY', 'IBM', 1000, 45.00),
             ('2006-04-05', 'BUY', 'MSFT', 1000, 72.00),
             ('2006-04-06', 'SELL', 'IBM', 500, 53.00),
            ]
c.executemany('INSERT INTO stocks VALUES (?,?,?,?,?)', purchases)
```



## 添加record

```
# Insert a row of data
c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")

# Save (commit) the changes
conn.commit()
```

```    
cursor.execute('''INSERT INTO records(T0, T1, duration, activity) VALUES(?,?,?,?)''', (T0, T1, duration, activity))

db.commit()
```

## 删除record



## 修改record
```
    cursor.execute('''UPDATE records SET activity = ? WHERE rowid = ? ''',
                   (new_activity, rowid))
```


## query

### display all

fetchone() + iterator

fetchall() 返回list

```
cursor.execute('''SELECT rowid, T0, T1, activity
                      FROM records''')
    all_records = cursor.fetchall()
    for record in all_records:
```


### 汇总

```    
cursor.execute('''SELECT activity, sum(duration) FROM records GROUP BY activity''')

all_rows = cursor.fetchall()
for activity in all_rows:
	pprint...
```

## 增加colume

[Insert new column into table in sqlite? - Stack Overflow](https://stackoverflow.com/questions/4253804/insert-new-column-into-table-in-sqlite)

## sqlite shell

[SQLite Show Tables: Listing All Tables in a Database](http://www.sqlitetutorial.net/sqlite-tutorial/sqlite-show-tables/)

## best practice

把常用transaction变成了function：
[Python SQLite3 tutorial (Database programming) | Codementor](https://www.codementor.io/likegeeks/python-sqlite3-tutorial-database-programming-riqdhwx9z)

高级特性：[charles leifer | Going Fast with SQLite and Python](http://charlesleifer.com/blog/going-fast-with-sqlite-and-python/)


## 常见要点

### 在web framework里，怎么组织conn最高效？

## 文献
[sqlite3 — DB-API 2.0 interface for SQLite databases — Python 3.7.2 documentation](https://docs.python.org/3.7/library/sqlite3.html)

[SQLite - Full Stack Python](https://www.fullstackpython.com/sqlite.html)

[Home · sqlitebrowser/sqlitebrowser Wiki](https://github.com/sqlitebrowser/sqlitebrowser/wiki) 可视化split

[membership.db/UserLogin.sql at master · membership/membership.db](https://github.com/membership/membership.db/blob/master/sqlite/UserLogin.sql)user seesion mgnt的sql实例

[SQLite Database System Design and Implementation (Second Edition, Version 2 ... - Sibsankar Haldar - Google 图书](https://books.google.ru/books?id=yWzwCwAAQBAJ)

[SQLite Documentation](https://www.sqlite.org/docs.html)

[User Profiles Data Model](http://www.databaseanswers.org/data_models/user_profiles/index.htm) databaseanswers有大量不同领域的实例和教程

[sql - Table Naming Dilemma: Singular vs. Plural Names - Stack Overflow](https://stackoverflow.com/questions/338156/table-naming-dilemma-singular-vs-plural-names/5841297#5841297)

[Tutorial - Approach to Database Design](http://www.databaseanswers.org/approach2db_design.htm)

[PEP 249 -- Python Database API Specification v2.0 | Python.org](https://www.python.org/dev/peps/pep-0249/)


SQL周边：[charles leifer | My List of Python and SQLite Resources](http://charlesleifer.com/blog/my-list-of-python-and-sqlite-resources/)
