# React 学习失败经验总结

我在学习 React 的时候走了一些弯路.总结如下:

-   没有完全掌握 Javascript.
-   没有掌握 ES6 的新 syntax.
-   看了太多基础视频,却没有及时利用小练习巩固内化.

所以如果未来你要学习 React 或类似前端,推荐的学习路径如下:

## JS 基础

完全新人: [JavaScript basics - Learn web development | MDN](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics)

有一定编程基础: [Introduction - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Introduction)

## JS 重点:

### Object/Prototype

JS 利用 prototype 建造对象,和 python 的 class 不一样.严格说 JS 没有 class.第一次阅读我用 python 那一套解释 JS object,越读越迷糊.后来及时知道了 prototype 的特定,直接阅读 MDN object oriented 文档依然很晕.直到我在网上搜索了 prototype 各组件之间的关系图示,一目了然.所以经验是:一图胜千言!

### this 和 bind

另外一个难理解的概念是 this.[你不知道的 JavaScript（上卷） (豆瓣)](https://book.douban.com/subject/26351021/)有详细解释.豆瓣上推荐了这本和[JavaScript (豆瓣)](https://book.douban.com/subject/2994925/).两本互相对照看,很快就能理解核心难点.

另外,Douglas Crockford 在 Youtube 上有许多演讲[视频](https://www.youtube.com/watch?v=JxAXlJEmNMg),风趣又犀利.

## React 基础

React[官方教程](https://reactjs.org/docs/hello-world.html)我一开始没看懂.补上空缺的知识点,每天看一点,慢慢开始 make sense 了.

最近正好在看 CS50,有个很酷的小哥给 Harvard 的新生介绍 React.节奏,清晰度和颜值,都完爆 YouTube 上各路 PO 主!

[React - CS50 Beyond 2019 - YouTube](https://www.youtube.com/watch?v=9NQtG_IIh6M&t=1771s)

[React, continued - CS50 Beyond 2019 - YouTube](https://www.youtube.com/watch?v=D7-Kc3umN5k&t=113s)

[Thinking in React - CS50 Beyond 2019 - YouTube](https://www.youtube.com/watch?v=9j2GIws5OQ4)

## 思考

对比在蟒营学 python 的经验,自学 React 的速度太慢.原因可能有:

-   缺少最小项目
-   没有明确每周任务/计划
-   学习路径安排不够合理
